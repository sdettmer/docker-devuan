#!/bin/sh -eu
# migrate.sh -- a Debian image to Devuan
# Copyright (C) 2017  Olaf Meeuwissen
#
# License: GPL-3.0+

IMAGE=$CI_REGISTRY_IMAGE/migrated:debian-$DEBIAN_VERSION
STAMP=$(date +%F)
SUITE=$(echo $DEBIAN_VERSION | sed 's/-.*//')

docker pull $IMAGE || true
docker build \
       --build-arg VERSION=$DEBIAN_VERSION \
       --build-arg SUITE=$SUITE \
       --tag $IMAGE-$STAMP \
       --file migrate.df .

docker push $IMAGE-$STAMP
docker tag  $IMAGE-$STAMP $IMAGE
docker push $IMAGE
